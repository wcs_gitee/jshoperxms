package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.JshopbasicInfoT;
import com.jshoperxms.service.JshopbasicInfoTService;

@Service("jshopbasicInfoTService")
@Scope("prototype")
public class JshopbasicInfoTServiceImpl extends BaseTServiceImpl<JshopbasicInfoT> implements JshopbasicInfoTService {
	
	
}
