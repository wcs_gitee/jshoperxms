package com.jshoperxms.action.mall.backstage.goods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.mall.backstage.vo.GoodsTypeBrandsVo;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.json.GsonJson;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.GoodsTypeBrandT;
import com.jshoperxms.service.BrandTService;
import com.jshoperxms.service.GoodsTypeBrandTService;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/mall/goods/goodstypebrand")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class GoodsTypeBrandTAction extends BaseTAction{
	@Resource
	private BrandTService brandTService;
	@Resource
	private GoodsTypeBrandTService goodsTypeBrandTService;

	/**
	 * 保存商品类型中的品牌
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String saveGoodsTypeBrandT(){
		if(StringUtils.isBlank(this.getBrands())){
			return JSON;
		}
		this.goodsTypeBrandTService.saveGoodsTypeBrands(this.getBrands(), StringUtils.trim(this.getGoodsTypeId()), StringUtils.trim(this.getGoodsTypeName()));
		this.setSucflag(true);
		return JSON;
	}


	/**
	 * 查询所有商品类型品牌
	 * @return
	 */
	@Action(value = "findByPage", results = { @Result(name = "json", type = "json") })
	public String findGoodsTypeBrandTByPage(){
		Map<String,Object>params=ActionContext.getContext().getParameters();
		findDefaultAllGoodsTypeBrandT();
		return JSON;
	}


	private void findDefaultAllGoodsTypeBrandT() {
		int currentPage=1;
		if(this.getStart()!=0){
			currentPage=this.getStart()/this.getLength()==1?2:this.getStart()/this.getLength()+1;
		}
		int lineSize = this.getLength();
		Criterion criterion=Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered=recordsTotal=this.goodsTypeBrandTService.count(GoodsTypeBrandT.class, criterion).intValue();
		List<GoodsTypeBrandT>list=this.goodsTypeBrandTService.findByCriteriaByPage(GoodsTypeBrandT.class, criterion, Order.desc("updatetime"), currentPage, lineSize);
		this.processGoodsTypeBrandTList(list);
	}


	private void processGoodsTypeBrandTList(List<GoodsTypeBrandT> list) {
		for(Iterator<GoodsTypeBrandT>it=list.iterator();it.hasNext();){
			GoodsTypeBrandT gt=it.next();
			Map<String,Object>cellMap=new HashMap<String, Object>();
			cellMap.put("id", gt.getGoodsTypeBrandTid());
			cellMap.put("goodsTypeName", gt.getGoodsTypeName());
			cellMap.put("brandname", gt.getBrandname());
			cellMap.put("status", DataUsingState.getName(gt.getStatus()));
			cellMap.put("updatetime", BaseTools.formateDbDate(gt.getUpdatetime()));
			cellMap.put("version", gt.getVersiont());
			data.add(cellMap);
		}
	}


	
	/**
	 * 删除商品类型品牌
	 * @return
	 */
	@Action(value = "del", results = { @Result(name = "json", type = "json") })
	public String delGoodsTypeBrandT(){
		String ids[]=StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for(String s:ids){
			GoodsTypeBrandT gtb=this.goodsTypeBrandTService.findByPK(GoodsTypeBrandT.class, s);
			if(gtb!=null){
				gtb.setUpdatetime(BaseTools.getSystemTime());
				gtb.setStatus(DataUsingState.DEL.getState());
				this.goodsTypeBrandTService.update(gtb);
			}
		}
		this.setSucflag(true);
		return JSON;
	}


	/**
	 * 批量删除的内容串
	 */
	private String ids;
	/**
	 * 品牌集合
	 */
	private String brands;
	private String brandid;
	private String brandname;
	private String goodsTypeId;
	private String goodsTypeName;

	private GoodsTypeBrandT bean;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered=0;
	/**
	 * 表格控件请求次数
	 */
	private int draw;
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public String getBrands() {
		return brands;
	}
	public void setBrands(String brands) {
		this.brands = brands;
	}
	public String getBrandid() {
		return brandid;
	}
	public void setBrandid(String brandid) {
		this.brandid = brandid;
	}
	public String getBrandname() {
		return brandname;
	}
	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}
	public String getGoodsTypeId() {
		return goodsTypeId;
	}
	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}
	public String getGoodsTypeName() {
		return goodsTypeName;
	}
	public void setGoodsTypeName(String goodsTypeName) {
		this.goodsTypeName = goodsTypeName;
	}
	public GoodsTypeBrandT getBean() {
		return bean;
	}
	public void setBean(GoodsTypeBrandT bean) {
		this.bean = bean;
	}
	public List<Map<String, Object>> getData() {
		return data;
	}
	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}
	public int getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean isSucflag() {
		return sucflag;
	}
	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

}
