package com.jshoperxms.action.mall.backstage.feedback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.enums.BaseEnums.DeviceType;
import com.jshoperxms.action.utils.enums.BaseEnums.FeedbackSource;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.FeedbackT;
import com.jshoperxms.service.FeedbackTService;
import com.jshoperxms.service.impl.Serial;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/mall/feedback")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class FeedbackTAction extends BaseTAction {

	private static final long serialVersionUID = 1L;

	@Resource
	private FeedbackTService feedbackTService;

	/**
	 * 保存反馈数据
	 * @return
	 */
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String saveFeedbackT() {
		FeedbackT ft = new FeedbackT();
		ft.setId(this.getSerial().Serialid(Serial.FEEDBACKT));
		ft.setContent(StringUtils.trim(this.getContent()));
		ft.setDevicetype(DeviceType.PC.getType());
		ft.setBasicinfoid(StringUtils.trim(this.getBasicinfoid()));
		ft.setDataid(StringUtils.trim(BaseTools.getAdminCreateId()));
		ft.setFeedbacksource(FeedbackSource.MALL.getSource());
		ft.setStatus(DataUsingState.USING.getState());
		ft.setUpdatetime(BaseTools.getSystemTime());
		ft.setCreatetime(BaseTools.getSystemTime());
		ft.setFeedbackpurpose(StringUtils.trim(this.getFeedbackpurpose()));
		ft.setVersiont(1);
		this.feedbackTService.save(ft);
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 查询所有反馈内容
	 * 
	 * @return
	 */
	@Action(value = "findAll", results = { @Result(name = "json", type = "json") })
	public String findAllFeedbackT() {
		Criterion criterion=Restrictions.eq("status", DataUsingState.USING.getState());
		List<FeedbackT> list = this.feedbackTService.findByCriteria(FeedbackT.class, criterion);
		if(!list.isEmpty()){
			beanlists = new ArrayList<FeedbackT>();
			beanlists = list;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 分页查询
	 * @return
	 */
	@Action(value = "findByPage", results = { @Result(name = "json", type = "json") })
	public String findFeedbackTByPage() {
//		Map<String, Object> params = ActionContext.getContext().getParameters();
//		for (Map.Entry<String, Object> entry : params.entrySet()) {
//			String key = entry.getKey();
//			Object value = entry.getValue();
//			System.out.println("key:" + key + " value: " + Arrays.toString((String[]) value));
//
//		}
		// 检测是否需要搜索内容
		findDefaultAllFeedbackTN();
		return JSON;
	}
	
	public void processFeedbackTList(List<FeedbackT> list) {
		for (Iterator<FeedbackT> it = list.iterator(); it.hasNext();) {
			FeedbackT ft = it.next();
			Map<String, Object> cellMap = new HashMap<String, Object>();
			cellMap.put("id", ft.getId());
			cellMap.put("content", ft.getContent());
			cellMap.put("devicetype", BaseEnums.DeviceType.getName(ft.getDevicetype()));
			cellMap.put("feedbacksource", BaseEnums.FeedbackSource.getName(ft.getFeedbacksource()));
			cellMap.put("feedbackpurpose", BaseEnums.FeedbackPurpose.getName(ft.getFeedbackpurpose()));
			cellMap.put("basicinfoid", ft.getBasicinfoid());
			cellMap.put("dataid", ft.getDataid());
			cellMap.put("updatetime", BaseTools.formateDbDate(ft.getUpdatetime()));
			cellMap.put("status", BaseEnums.DataUsingState.getName(ft.getStatus()));
			data.add(cellMap);

		}
	}

	public void findDefaultAllFeedbackTN() {
		int currentPage=1;
		if(this.getStart()!=0){
			currentPage=this.getStart()/this.getLength()==1?2:this.getStart()/this.getLength()+1;
		}
		int lineSize = this.getLength();
		Criterion criterion=Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered = recordsTotal = this.feedbackTService.count(FeedbackT.class, criterion).intValue();
		List<FeedbackT> list = this.feedbackTService.findByCriteriaByPage(FeedbackT.class, criterion, Order.desc("updatetime"), currentPage, lineSize);
		this.processFeedbackTList(list);
	}

	/**
	 * 根据反馈id查询内容
	 * 
	 * @return
	 */
	@Action(value = "find", results = { @Result(name = "json", type = "json") })
	public String findOneFeedbackT() {
		if (StringUtils.isBlank(this.getFeedbackId())) {
			return JSON;
		}
		FeedbackT feedbackT = this.feedbackTService.findByPK(FeedbackT.class, this.getFeedbackId());
		if (feedbackT != null) {
			bean = feedbackT;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 更新反馈数据
	 * @return
	 */
	@Action(value="update", results = { @Result(name = "json", type="json")})
	public String updateFeedbackT() {
		if(StringUtils.isBlank(this.getFeedbackId())){
			return JSON;
		}
		FeedbackT feedbackT = this.feedbackTService.findByPK(FeedbackT.class, this.getFeedbackId());
		feedbackT.setContent(StringUtils.trim(this.getContent()));
		feedbackT.setBasicinfoid(StringUtils.trim(this.getBasicinfoid()));
		feedbackT.setStatus(StringUtils.trim(this.getStatus()));
		feedbackT.setUpdatetime(BaseTools.getSystemTime());
		feedbackT.setFeedbackpurpose(StringUtils.trim(this.getFeedbackpurpose()));
		feedbackT.setVersiont(feedbackT.getVersiont()+1);
		this.feedbackTService.update(feedbackT);
		this.setSucflag(true);
		return JSON;
	}
	
	/**
	 * 删除反馈数据
	 */
	@Action(value = "del", results = { @Result(name = "json", type = "json") })
	public String delFeedbackT() {
		String ids[] = StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for (String s : ids) {
			FeedbackT feedbackT = this.feedbackTService.findByPK(FeedbackT.class, s);
			if (feedbackT != null) {
				feedbackT.setStatus(DataUsingState.DEL.getState());
				feedbackT.setUpdatetime(BaseTools.getSystemTime());
				this.feedbackTService.update(feedbackT);
			}
		}
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 批量删除的内容串
	 */
	private String ids;
	private String feedbackId;
	private String content;
	private String basicinfoid;
	private String dataid;
	private String feedbacksource;
	private String feedbackpurpose;
	private String status;
	private FeedbackT bean;
	private List<FeedbackT> beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered = 0;
	private int draw;// 表格控件请求次数
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public FeedbackTService getFeedbackTService() {
		return feedbackTService;
	}

	public void setFeedbackTService(FeedbackTService feedbackTService) {
		this.feedbackTService = feedbackTService;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getBasicinfoid() {
		return basicinfoid;
	}

	public void setBasicinfoid(String basicinfoid) {
		this.basicinfoid = basicinfoid;
	}

	public String getDataid() {
		return dataid;
	}

	public void setDataid(String dataid) {
		this.dataid = dataid;
	}

	public String getFeedbacksource() {
		return feedbacksource;
	}

	public void setFeedbacksource(String feedbacksource) {
		this.feedbacksource = feedbacksource;
	}

	public String getFeedbackpurpose() {
		return feedbackpurpose;
	}

	public void setFeedbackpurpose(String feedbackpurpose) {
		this.feedbackpurpose = feedbackpurpose;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(String feedbackId) {
		this.feedbackId = feedbackId;
	}

	public FeedbackT getBean() {
		return bean;
	}

	public void setBean(FeedbackT bean) {
		this.bean = bean;
	}

	public List<FeedbackT> getBeanlists() {
		return beanlists;
	}

	public void setBeanlists(List<FeedbackT> beanlists) {
		this.beanlists = beanlists;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

}
