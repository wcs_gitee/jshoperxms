﻿// DomReady
$(function(){

	// 全局下拉菜单
	initSubMenu();

	// 精品推荐切换
	promoSwitch();

	//分页
	$("#GoPage").bind("keydown", function () {
	  if (event.keyCode == 13) {
	    $('#GoPageBtn').click();
	    return false;
	  }
	})

	if ($('#GoPageBtn').length) {
	  $('#GoPageBtn').click(function () {
	    if ($('#GoPage').val() != "") {
	      var count = parseInt($('#_PageCount').html());
	      if (!isNaN($('#GoPage').val())) {
	        if ((parseInt($('#GoPage').val()) > count) || (parseInt($('#GoPage').val()) < 1)) {
	          alert("页码超出索引，请重新输入！");
	          return false;
	        }
	      }
	      else {
	        alert("页码输入错误！");
	        return false;
	      }
	    }
	    else {
	      alert("请输入页码！");
	      return false;
	    }
	  });
	}

});


function initSubMenu(){
	var timer, cur;
	var menu = $('.menu li a'),
		subm = $('#subMenu'),
		item = $('.submenu', subm);

	cur = menu.filter('.cur').attr('rel');

	menu.mouseover(function(){
		var me = $(this);
		var pos = parseInt(me.attr('rel')) - 1;
		menu.removeClass('cur');
		if (timer) clearTimeout(timer);
		if (pos < 0) {
			subm.hide();
			return;
		}
		subm.show();
		item.hide().eq(pos).show();
		me.addClass('cur');
	}).mouseout(function(){
		hide();
	});

	item.mouseover(function(){
		if (timer) clearTimeout(timer);
	}).mouseout(function(){
		hide();
	});

	function hide(){
		timer = setTimeout(function(){
			subm.hide();
			menu.removeClass('cur');
			menu.eq(cur).addClass('cur');
		}, 200);
	}
}

function promoSwitch(){
	var timer;
	var promo = $('#Left_PubLeft_promotion'),
		promoTab = $('.item a', promo),
		promoInf = $('.inf', promo);
	// 鼠标进过延时200ms 触发切换
	promoTab.mouseover(function () {
	    if (timer) clearTimeout(timer);
	    var i = parseInt($(this).attr('href').replace(/#/, '')) - 1;
	    timer = setTimeout(function () {
	        if (i == 0) {
	            promo.find(".item").removeClass('ot');
	            promo.find(".item").addClass('cur');
	            promo.find(".bot").css("background", "url(promo_box.jpg) left bottom");
	        }
	        else {
	            promo.find(".item").removeClass('cur');
	            promo.find(".item").addClass('ot');
	            promo.find(".bot").css("background", "url(promo_box.jpg) -174px bottom");
	        }
	        promoInf.hide().eq(i).show();
	    }, 200);
	    return false;
	}).click(function () {
	    return false;
	});
	promo.mouseout(function(){
		if (timer) clearTimeout(timer);
	});
}

function scrollPanel(op){

	op = op || {};

	var box = $('#scrollPanel'),
		panel = $('.panel', box),
		inner = $('.inner', box),
		item = $('.item', box),
		prev = $('#btnPrev'),
		next = $('#btnNext'),
		len = item.length,
		timer = null,
		lock = false,
		width = op.width || 283,
		maxw = width * len,
		cur = 0;

	init();

	function init(){
		var html = inner.html();
		// 无缝拼接
		inner.html(html + html);
		// 初始归位
		panel.scrollLeft(0);
		// 点击事件
		prev.click(function(){
			scroll(-1);
		});
		next.click(function(){
			scroll(1);
		});
		// 初始化事件回调
		if (op.callback) op.callback(box);
	}

	function scroll(f){
		if (lock) return;
		if (f > 0) {
			cur++;
		} else {
			cur--;
		}
		if (cur < 0) {
			cur = len - 1;
			panel.scrollLeft(maxw);
		}
		if (cur > len) {
			cur = 1;
			panel.scrollLeft(0);
		}
		lock = true;
		panel.animate({ scrollLeft: width * cur }, 700, 'easeInOutQuad', function(){
			lock = false;
		});
	}
}

function chgYZM(){
    setTimeout(refreshimg, 1);
}

function refreshimg(){
    var s = "/Controller/ValidateCode.aspx?" + Math.random();
    $("#yzm").attr("src", s);
}

function JHshNumberText(){
    if (!(((window.event.keyCode >= 48) && (window.event.keyCode <= 57))
		|| (window.event.keyCode == 13) || (window.event.keyCode == 46)
		|| (window.event.keyCode == 45)))
	{
        window.event.keyCode = 0;
    }
}

/**
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 * @note jQuery动画方法的缓冲效果
 */
jQuery.extend( jQuery.easing, {
	// t: cur time, b: begInnIng value, c: change In value, d: duration
	easeInOutQuad: function (x, t, b, c, d) { //===easeInOutCubic
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	}
});



/*浮动登录*/
function ShowFlowLoginDiv() {
    $("#OverFlow").show();
    $("#FlowLoginDiv").show();
}
function HideFlowLoginDiv() {
    $("#OverFlow").hide();
    $("#FlowLoginDiv").hide();
}

/*浮动登录*/
function ShowFlowLoginDiv() {
    $("#OverFlow").show();
    $("#FlowLoginDiv").show();
}
function HideFlowLoginDiv() {
    $("#OverFlow").hide();
    $("#FlowLoginDiv").hide();
}

function ShowApplicationDiv() {
    $("#OverFlow").show();
    $("#FlowApplicationDiv").show();
}
function HideApplicationDiv() {
    $("#OverFlow").hide();
    $("#FlowApplicationDiv").hide();
}

/*下载提示*/
function ShowTip() {
    $("#OverFlow").show();
    $("#TipDiv").show();
}
function HideTip() {
    $("#OverFlow").hide();
    $("#TipDiv").hide();
}

function ShowTip1() {
    $("#OverFlow").show();
    $("#TipDiv1").show();
}
function HideTip1() {
    $("#OverFlow").hide();
    $("#TipDiv1").hide();
}

function ShowTip2() {
  $("#OverFlow").show();
  $("#TipDiv2").show();
}
function HideTip2() {
  $("#OverFlow").hide();
  $("#TipDiv2").hide();
}

function getCookie(cookie_name) {
  var allcookies = document.cookie;
  var cookie_pos = allcookies.indexOf(cookie_name);
  // 如果找到了索引，就代表cookie存在，
  // 反之，就说明不存在。
  if (cookie_pos != -1) {
    // 把cookie_pos放在值的开始，只要给值加1即可。
    cookie_pos += cookie_name.length + 1;
    var cookie_end = allcookies.indexOf(";", cookie_pos);
    if (cookie_end == -1) {
      cookie_end = allcookies.length;
    }
    var value = unescape(decodeURI(allcookies.substring(cookie_pos, cookie_end)));
  }
  return value;
}
