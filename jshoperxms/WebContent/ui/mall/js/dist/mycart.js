 $(function () {
             //总数
             $(".quanxun").click(function () {
                 setTotal();
                 //alert($(lens[0]).text());
             });
            function setTotal() {
                 var len = $(".tot");
                  var num = 0;
                 for (var i = 0; i < len.length; i++) {
                     num = parseInt(num) + parseInt($(len[i]).text());
                  }
                  //alert(len.length);
                  $("#zong1").text(parseInt(num).toFixed(2));
                 $("#shuliang").text(len.length);
              };

              //将商品加入购物车
              addgoodstocart = function(basepath,goodId,goodsnum){
                    //商品数量
                    var needquantity=0;
                    var godsnum = goodsnum;
                    if (godsnum != undefined && godsnum>0) {
                          needquantity = godsnum;
                    }else{
                          needquantity = $("#quantity").val();
                          if (needquantity == undefined || needquantity <= 0) {
                              return false;
                          }
                    }
                    //商品id
                     var goodsid = "";
                    if (goodId  != undefined && goodId!="") {
                          goodsid = goodId;                          
                    }else{
                          goodsid = $("#hidgoodsid").val();
                          if (goodsid == undefined || goodsid=="") {
                              return false;
                          }
                    }
                
                  
                    var orderTag = "1"; // 普通商品
                    $.post(basepath+"/addCart.action",{
                              "goodsid" : goodsid,
                              "needquantity" : needquantity,
                              "orderTag" : orderTag
                            },function(data) {
                              if (!data.slogin) {
                                // 跳转到登录页面
                                window.location.href = basepath+ "/gologin.action";
                              } else {
                                if (data.sucflag) {
                                  // 跳转到购物车页面

                                  window.location.href = basepath+"/gotocartpage.action";
                                } else {
                                  // 跳转到登录页面
                                  window.location.href = basepath+ "/gologin.action";
                                }
                              }
                    })
              };
              //提交订单是修改相关的数据
             subminOrder =  function(basepath){
                      //收集页面数据
                      var sum =0;
                      var sendstr = "";
                      $(":checkbox[name='newslist']").each(function() {
                      if ($(this).attr("checked")) {
                        sum++;
                        var array = $(this).val().split(",");
                        sendstr +=array[0]+","+array[1]+":";
                      }
                    });
                    if (sum == 0) {
                      alert('请勾选商品');
                      return false;
                    }
                    // alert(sendstring);
                    //ajax  PlusCartNeedquantityByGoodsid
                      // var datajson = {"sendstring":sendstring};
                    $.post(basepath+"/PlusCartNeedquantityByGoodsid.action",
                                {
                                  "sendstring":sendstr
                                },
                            function(data){
                                if(data.sucflag){
                                        window.location.href=basepath+"/order/initOrder.action";
                                  }
                            });
             };
             //删除购物车中的一条数据
             deleteCartById = function(basepath,id){
                   $.post(basepath+"/delCartByid.action",
                                {
                                  "id":id
                                },
                            function(data){
                                if(data.sucflag){
                                        window.location.href=basepath+"/gotocartpage.action";
                                  }
                            });
                  
             };
})